from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def dump_headers():
    headers = ["{}: {}".format(k, v) for k, v in request.headers.items()]
    return "\n".join(headers)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
